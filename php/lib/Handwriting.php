<?php

define('HANDWRITING_API_HOST', 'https://us-central1-hc-application-interface-prod.cloudfunctions.net/api/v1');

define('HANDWRITING_STYLE_WHITMAN', 'Whitman');
define('HANDWRITING_STYLE_DICKINSON', 'Dickinson');
define('HANDWRITING_STYLE_PLATH', 'Plath');
define('HANDWRITING_STYLE_GINSBERG', 'Ginsberg');
define('HANDWRITING_STYLE_EMERSON', 'Emerson');

define('PAPER_SIZE_A4', 'A4');
define('PAPER_SIZE_A5', 'A5');
define('PAPER_SIZE_A5_LANDSCAPE', 'A5_landscape');

define('CURRENCY_GBP', 'GBP');
define('CURRENCY_USD', 'USD');

define('HEADER_TYPE_LOGO', 'Logo');
define('HEADER_TYPE_TEXT', 'Text');

define('FONT_JOSEFIN', 'josefin');
define('FONT_PLAYFAIR', 'playfair');
define('FONT_POIRET', 'poiret');

define('PRODUCT_ADVANCED', 'Full Service');
define('PRODUCT_ON_DEMAND', 'Self Service');
define('PRODUCT_ROBOTICS', 'Robotics');

define('CAMPAIGN_STATUS_PENDING', 'Pending');
define('CAMPAIGN_STATUS_IN_PROGRESS', 'In progress');
define('CAMPAIGN_STATUS_COMPLETE', 'Complete');
define('CAMPAIGN_STATUS_ERROR', 'Error');

define('INSERT_AMAZON_5_DOLLARS', 'amazon_5_dollars');
define('INSERT_AMAZON_10_DOLLARS', 'amazon_10_dollars');
define('INSERT_AMAZON_15_DOLLARS', 'amazon_15_dollars');
define('INSERT_AMAZON_20_DOLLARS', 'amazon_20_dollars');
define('INSERT_VISA_5_DOLLARS', 'visa_5_dollars');
define('INSERT_VISA_10_DOLLARS', 'visa_10_dollars');
define('INSERT_VISA_15_DOLLARS', 'visa_15_dollars');
define('INSERT_VISA_20_DOLLARS', 'visa_20_dollars');
define('INSERT_STARBUCKS_5_DOLLARS', 'starbucks_5_dollars');
define('INSERT_STARBUCKS_10_DOLLARS', 'starbucks_10_dollars');
define('INSERT_STARBUCKS_15_DOLLARS', 'starbucks_15_dollars');
define('INSERT_STARBUCKS_20_DOLLARS', 'starbucks_20_dollars');

class HandwritingException extends Exception
{
    public $serverResponse;
    public $responseCode;
    public $errorCode;
    public $errorMessage;

    public function __construct($message, $responseCode=null, $serverResponse=null)
    {
        $this->responseCode = $responseCode;
        $this->serverResponse = $serverResponse;

        if (isset($serverResponse->error_message)) {
            $this->errorMessage = $serverResponse->error_message;
            if (isset($serverResponse->error_code)) {
                $this->errorCode = $serverResponse->error_code;
            }
            parent::__construct($serverResponse->error_message);
        } else {
            parent::__construct($message);
        }
    }
}

class Handwriting
{
    private static $apiKey;
    private static $isTesting;
    private static $curlHandle;

    public static function init($apiKey, $isTesting)
    {
        self::$apiKey = $apiKey;
        self::$isTesting = $isTesting;
    }

    public static function test()
    {
        return 15;
    }

    public static function BuildUrl($object)
    {
        return HANDWRITING_API_HOST . '/' . $object;
    }

    public function Request($requestMethod, $url, $payload=null)
    {
        self::$curlHandle = curl_init();

        $url = $url . "?api_key=" . self::$apiKey .  "&testing=" . self::$isTesting;

        $payload_string = json_encode($payload);

        curl_setopt(self::$curlHandle, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt(self::$curlHandle, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt(self::$curlHandle, CURLOPT_RETURNTRANSFER, 1);

        curl_setopt(self::$curlHandle, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
        curl_setopt(self::$curlHandle, CURLOPT_CUSTOMREQUEST, $requestMethod);

        if ($requestMethod == "POST") {
            curl_setopt(self::$curlHandle, CURLOPT_POST, 1);
            curl_setopt(self::$curlHandle, CURLOPT_POSTFIELDS, $payload_string);
        }

        curl_setopt(self::$curlHandle, CURLOPT_URL, $url);

        $result = curl_exec(self::$curlHandle);

        if (curl_getinfo(self::$curlHandle, CURLINFO_HTTP_CODE) !== 200 || !json_decode($result)->success) {
            throw new HandwritingException(curl_error(self::$curlHandle), curl_getinfo(self::$curlHandle, CURLINFO_HTTP_CODE), json_decode($result));
        }

        return json_decode($result);
    }

    public static function DeleteCampaign($id)
    {
        $payload = array(
            "campaign_id" => $id,
        );

        return self::Request('DELETE', self::BuildUrl('campaign/'.$id), $payload);
    }

    public static function GetCampaign($id)
    {
        $payload = array(
            "campaign_id" => $id,
    );

        return self::Request('GET', self::BuildUrl('campaign/'.$id), $payload);
    }

    public static function CreateCampaign($payload)
    {
        return self::Request('POST', self::BuildUrl('campaign'), $payload);
    }

    public static function PriceCampaign($payload)
    {
        return self::Request('POST', self::BuildUrl('price'), $payload);
    }
}
