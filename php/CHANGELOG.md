# Change log
## 04/03/20
- Point towards new endpoint
- TODO please sign up for an account on https://app.scribeless.co/

## 27/05/19
- Added additional handwriting styles, can be found in `lib/Handwriting.php`
- Add header/footer text/logo
  - Either header text or logo can be added when creating a campaign
    ```
    "header_image" => "http://app.thehandwriting.company/static/images/your_logo.png",
    "header_type" => HEADER_TYPE_LOGO,
    ```
    Or
    ```
    "header_type" => HEADER_TYPE_TEXT,
    "header_font" => FONT_OSEFIN,
    "header_text" => "Header text"
    ```
  - Footer text can be added when creating a campaign
    ```
    "footer_text" => "Footer text",
    "footer_font" => FONT_OSEFIN,
    ```
  - Header/footer fonts can be found in `lib/Handwriting.php`
  - Logos are resized to be 20mm tall and placed top centre on the paper
  - The resulting stationary can be previewed by the url in the `stationary_thumbnail` parameter when the API returns
- Added `/price` endpoint to calculate price before submitting a campaign
  - See `examples/PriceCampaign.php`
- Added `currency` parameter, available currencies are found in `lib/Handwriting.php`
  - `currency` is returned from API along with cost
  - **NOTE** That default currency is GBP
