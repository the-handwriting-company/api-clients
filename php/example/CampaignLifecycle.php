<?php
/*
Tests the complete lifecycle of a campaign. Creates, retrieves, and then delete a campaign
*/

// This file holds API keys for the demos
require_once('Constants.php');
// Include the Handwriting API
require_once('../lib/Handwriting.php');

// Initialize the API with API key
Handwriting::init(HANDWRITING_API_KEY, HANDWRITING_TESTING);

$recipient = array(
  "address line 1" => "Flat 1",
  "address line 2" => "123 Broom Road",
  "address line 3" => "Bathwick hill",
  "city" => "London",
  "country" => "United Kingdom",
  "department" => "HR",
  "first name" => "Tim",
  "last name" => "Johnson",
  "state/region" => "London",
  "title" => "Mr",
  "zip/postal code" => "TW11 9PG"
);

$returnAddress = array(
    "address line 1" => "Flat 1",
    "address line 2" => "123 Broom Road",
    "address line 3" => "Bathwick hill",
    "city" => "London",
    "country" => "United Kingdom",
    "department" => "HR",
    "first name" => "Tim",
    "last name" => "Johnson",
    "state/Region" => "London",
    "title" => "Mr",
    "zip/postal code" => "TW11 9PG"
);

$payload = array(
    "paper_size" => PAPER_SIZE_A5,
    "handwriting_style" => HANDWRITING_STYLE_PLATH,
    'title' => 'Campaign title', // Optional
    'product' => PRODUCT_ADVANCED, // Optional, Default product
    'text' => "Dear Alex,\nWhen was the last time you received a handwritten note?\nKind regards,\nAlex",
    'inserts' => INSERT_AMAZON_10_DOLLARS, // Optional, Leave blank for no insert
    //'due_date' => '14/11/2019', // Optional, Leave blank for ASAP
    'notes' => 'Notes', // Optional
    "return_address" => $returnAddress, // Optional
    "recipients" => array($recipient),
    "currency" => CURRENCY_USD, // Optional
    "header_image" => "https://app.scribeless.co/static/images/your_logo.png", // Optional
    "header_type" => HEADER_TYPE_LOGO, // Optional
    // "header_font" => FONT_OSEFIN,
    // "header_text" => "Header text",
    "footer_text" => "Footer text", // Optional, leave blank for no footer
    "footer_font" => FONT_JOSEFIN, // Optional
);

try {
  echo "Creating campaign...\n";
  $campaign = Handwriting::CreateCampaign($payload);
  var_dump($campaign);

  echo "\n\nGetting campaign " . $campaign->id . "...\n";
  $result = Handwriting::GetCampaign($campaign->id);
  var_dump($result);

  echo "\n\nDeleting campaign " . $campaign->id . "...\n";
  $result = Handwriting::DeleteCampaign($campaign->id);
  var_dump($result);
}
catch(Exception $e) {
  echo 'Caught exception: ',  $e->getMessage(), "\n";
  echo 'Error code: ', $e->errorCode, "\n";
}
