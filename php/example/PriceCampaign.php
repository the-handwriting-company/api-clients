<?php
/*
Prices a new campaign
*/

// This file holds API keys for the demos
require_once('Constants.php');
// Include the Handwriting API
require_once('../lib/Handwriting.php');
// Initialize the API with API key
Handwriting::init(HANDWRITING_API_KEY, HANDWRITING_TESTING);

$recipient = array(
  "address line 1" => "Flat 1",
  "address line 2" => "123 Broom Road",
  "address line 3" => "Bathwick hill",
  "city" => "London",
  "country" => "United Kingdom",
  "department" => "HR",
  "first name" => "Tim",
  "last name" => "Johnson",
  "state/region" => "London",
  "title" => "Mr",
  "zip/postal code" => "TW11 9PG"
);

$returnAddress = array(
    "address line 1" => "Flat 1",
    "address line 2" => "123 Broom Road",
    "address line 3" => "Bathwick hill",
    "city" => "London",
    "country" => "United Kingdom",
    "department" => "HR",
    "first name" => "Tim",
    "last name" => "Johnson",
    "state/Region" => "London",
    "title" => "Mr",
    "zip/postal code" => "TW11 9PG"
);

$payload = array(
    "paper_size" => PAPER_SIZE_A5,
    "handwriting_style" => HANDWRITING_STYLE_PLATH,
    'title' => 'Campaign title', // Optional
    'product' => PRODUCT_ADVANCED, // Optional, Default product
    'text' => "Dear Alex,\nHow is it going?\nKind regards,\nAlex",
    // 'inserts' => INSERT_AMAZON_10_DOLLARS, // Optional, Leave blank for no insert
    //'due_date' => '14/11/2019', // Optional, Leave blank for ASAP
    'notes' => 'Notes', // Optional
    "return_address" => $returnAddress, // Optional
    "recipients" => array($recipient),
    "currency" => "gbp", // Optional
    "header_image" => "https://app.scribeless.co/static/images/your_logo.png", // Optional
    "header_type" => HEADER_TYPE_LOGO, // Optional
    // "header_font" => FONT_OSEFIN,
    // "header_text" => "Header text",
    "footer_text" => "Footer text", // Optional, leave blank for no footer
    "footer_font" => FONT_JOSEFIN, // Optional
);

try {
  $campaign = Handwriting::PriceCampaign($payload);
  var_dump($campaign);
}
 catch(Exception $e) {
  echo 'Caught exception: ',  $e->getMessage(), "\n";
  echo 'Error code: ', $e->errorCode, "\n";
}
