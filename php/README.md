# The Handwriting Company PHP API Client
[API Documentation](https://support.scribeless.co/api)

## Use
The API can be used in sandbox mode by setting `testing` (`HANDWRITING_TESTING` in `example/Constants.php`). Sandbox mode requires a credit card to be inserted but will not make charges to the card.

1. Get your `API_KEY` see [documentation](https://support.scribeless.co/find-your-api-key). You will need to make an account on https://app.scribeless.co
2. Edit `example/Constants.php` replace `YOUR API KEY` with your api key
3. Run
  ```
  $ cd example
  $ php CampaignLifecycle.php
  ```

> Note that campaigns can be viewed on the web interface via https://app.thehandwriting.company/campaign/<CAMPAIGN_ID>

## Production
To use in production disable the sandbox mode by editing `example/Constants.php` set `HANDWRITING_TESTING` to `false`
