const {Handwriting, CONSTANTS} = require('./lib/the_handwriting_company.js')

 // TODO
const API_KEY = 'YOUR API KEY'
const IS_TESTING = true

async function main() {
  var handwriting_client = new Handwriting(API_KEY, IS_TESTING)

  console.log('Create')
  var data = {
    'paper_size': CONSTANTS.paper_size.A5_landscape,
    'handwriting_style': CONSTANTS.handwriting_style.ernest,
    'title': 'Campaign title', // Optional
    'product': CONSTANTS.product.advanced, // Default product
    'text': "Dear Alex,\nHow is it going?\nKind regards,\nAlex",
    'inserts': CONSTANTS.insert.amazon_5_dollars, // Leave blank for no insert
    'due_date': '14/11/2019', // Optional, Leave blank for ASAP
    'notes': 'Notes', // Optional
    'return_address': { // Optional
      "address line 1": "Flat 1",
      "address line 2": "123 Broom Road",
      "address line 3": "Bathwick hill",
      "city": "London",
      "country": "United Kingdom",
      "department": "HR",
      "first name": "Tim",
      "last name": "Johnson",
      "state/Region": "London",
      "title": "Mr",
      "zip/postal code": "TW11 9PG"
    },
    "recipients": [
        {
            "address line 1": "Flat 1",
            "address line 2": "123 Broom Road",
            "address line 3": "Bathwick hill",
            "city": "London",
            "country": "United Kingdom",
            "department": "HR",
            "first name": "Tim",
            "last name": "Johnson",
            "state/region": "London",
            "title": "Mr",
            "zip/postal code": "TW11 9PG"
        }
    ],
  }

  var response = await handwriting_client.create_campaign(data)
  console.log(response)

  if (!response.success) {
    console.log('Failed to create a campaign, exiting')
    return
  }

  var campaign_id = response.id

  console.log(`Campaign ${response.id} will cost ${response.cost} has status ${response.status}`)

  console.log('Get')
  var response = await handwriting_client.get_campaign(campaign_id)
  console.log(response)
  console.log(CONSTANTS.campaign_status.pending)

  console.log('Delete')
  var response = await handwriting_client.delete_campaign(campaign_id)
  console.log(response)
}

main()
