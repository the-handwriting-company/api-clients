# The Handwriting Company NodeJS API Client
[API Documentation](https://support.thehandwriting.company/portal/kb/articles/api-endpoints)

## Use
The API can be used in sandbox mode by setting `testing` (`const IS_TESTING = false` in `example/example.js`). Sandbox mode requires a credit card to be inserted but will not make charges to the card.

1. Get your `API_KEY` see [documentation](https://support.thehandwriting.company/portal/kb/articles/api)
2. Edit `example/example.js` replace `YOUR API KEY` with your api key
3. Run `$ node example/example.js`

> Note that campaigns can be viewed on the web interface via https://app.thehandwriting.company/campaign/<CAMPAIGN_ID>

## Production
To use in production disable the sandbox mode by editing `example/example.js` set `const IS_TESTING = false`
