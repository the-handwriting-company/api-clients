const request = require('request-promise');

// const BASE_URL = 'https://us-central1-unearnest-clientside-interface.cloudfunctions.net/'
const BASE_URL = 'http://localhost:5001/unearnest-clientside-interface/us-central1/' // TODO

const CONSTANTS = {
  campaign_status: {
    'pending': 'Pending',
    'in_progress': 'In progress',
    'complete': 'Complete',
    'error': 'Error',
  },
  paper_size: {
    'A4': 'A4',
    'A5': 'A5',
    'A5_landscape': 'A5_landscape',
  },
  product: {
    'advanced': 'Advanced',
    'on_demand': 'On Demand'
  },
  insert: {
    'amazon_5_dollars': 'amazon_5_dollars',
    'amazon_10_dollars': 'amazon_10_dollars',
    'amazon_15_dollars': 'amazon_15_dollars',
    'amazon_20_dollars': 'amazon_20_dollars',
    'visa_5_dollars': 'visa_5_dollars',
    'visa_10_dollars': 'visa_10_dollars',
    'visa_15_dollars': 'visa_15_dollars',
    'visa_20_dollars': 'visa_20_dollars',
    'starbucks_5_dollars': 'starbucks_5_dollars',
    'starbucks_10_dollars': 'starbucks_10_dollars',
    'starbucks_15_dollars': 'starbucks_15_dollars',
    'starbucks_20_dollars': 'starbucks_20_dollars',
  },
  handwriting_style: {
    'jane': 'Jane',
    'ernest': 'Ernest',
    'mark': 'Mark',
    'virginia': 'Virginia',
    'william': 'William',
    'george': 'George',
  },
  error_code: {
    'auth_unauthorised': 'auth/unauthorised',
    'error_field_required': 'error/field-required',
    'error_invalid_status': 'error/invalid-status',
    'error_not_found': 'error/not-found',
    'error_payment_method_required': 'error/payment-method-required',
    'error_invalid_date': 'error/invalid-date',
    'error_invalid_handwriting_style': 'error/invalid-handwriting-style',
    'error_invalid_paper_size': 'error/invalid-paper-size',
    'error_unknown_endpoint': 'error/unknown-endpoint',
    'error_invalid_key': 'error/invalid-key',
    'error_invalid_product': 'error/invalid-product',
  }
}

class Handwriting {
  constructor(api_key, testing) {
    this.api_key = api_key
    this.testing = testing
  }

  _make_request(endpoint, method, data) {
    data['testing'] = this.testing
    data['api_key'] = this.api_key

    // console.log(data, `${BASE_URL}${endpoint}`, method)

    return request({
      method: method,
      uri: `${BASE_URL}${endpoint}`,
      body: data,
      json: true,
      headers: {'Content-Type': 'application/json'}
    }).catch((response) => {
      // console.log(response.name)
      // console.log(response.statusCode)
      // console.log(response.error.error_message)
      // console.log(response.error.error_code)
      return response.error
    })
  }

  create_campaign(data) {
    return this._make_request('campaign', 'POST', data)
  }

  get_campaign(campaign_id) {
    if (!campaign_id) {
      return {success: false, error_message: 'campaign_id field is required', error_code: CONSTANTS.error_code.error_field_required}
    }

    var data = {
      'campaign_id': campaign_id
    }

    return this._make_request('get_campaign', 'POST', data)
  }

  delete_campaign(campaign_id) {
    if (!campaign_id) {
      return {success: false, error_message: 'campaign_id field is required', error_code: CONSTANTS.error_code.error_field_required}
    }

    var data = {
      'campaign_id': campaign_id
    }

    return this._make_request('campaign', 'DELETE', data)
  }
}

module.exports = {
  Handwriting,
  CONSTANTS: CONSTANTS
}
